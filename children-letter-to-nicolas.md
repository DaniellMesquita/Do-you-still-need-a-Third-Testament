# Children: letter to Nicolas

### CHAPTER 1

1. Letter addressed to those who have a great deal to know about God, for being less than 11 years old;
2. But who has superior and sufficient intellect so that he may read and understand great mysteries.
3. You remind me a lot when at your age;
4. Personality, thoughts, tastes, etc.
5. You answered your brother Leonardo when he said, "I am Jesus," and responded very well. He responded with authority.
6. And to God it does not matter if you are Mormons, if you follow in Spirit and in truth.
7. Because for God there is no difference between black and white, agnostic and spiritist, catholic and evangelical, testimonies of Jehovah and Mormons;
8. Everyone has the opportunity to follow the same truth, which is independent of religions.
9. And I relune the religions.
10. Once you said, "If this beast is not of God, why, if God created?"
11. All animals are of God;
12. But the serpent is a symbol of the devil, who used the form to try.
13. Then why did God create bad things?
14. Why did he create venomous animals that poison people?
15. Because here is not heaven, and the best things God holds as treasure. For us. And we will love it.
16. We know the bad things on earth, to admire even more the good things in heaven, when we can say:
17. "Look at this, on Earth it was not like that, it's worth what we live there".
18. But we can not want to die: our life, of us humans here, is sacred.
19. It is a test, in which an unforgivable sin commits one to give up;
20. But those who overcome will be crowned and crowned.
21. Live intensely, but be careful not to sin.
22. Then saidst thou:
23. "But everyone is a sinner, so everyone goes to hell?" But not all are sinners because they will;
24. For he sinneth against God, he that sinneth, knowing he is sinning.
25. But we are sinners by simply sinning.
26. But God says that we must seek to perfect ourselves, as Christ is perfect.
27. And we must seek to be holy, even if we are mocked for being different;
28. But do not feel bad if you can not, because Christ Jesus prefers those who are not afraid.

### CHAPTER 2

1. You are very agitated, and this agitation brings you hunger for learning, intelligence and creativity;
2. And one day you may use this to announce the gospel \(the word of God\) to all.
3. But it will not do any good \(neither for you nor for those who are evangelized\) if you do not have faith and especially love.
4. What does the gospel mean?
5. Is it evangelical?
6. Thing of "believer"? Not;
7. Because even Catholics, and Spiritists, and Mormons, and testimonies of Jehovah; these also evangelize.
8. Praying and praying are the same \(although each religion erects its labels\), as missions and evangelizing are intertwined.
9. Gospel means: good news.
10. The gospel did not come from the gospel religion, but the gospel religion comes from the gospel.
11. But evangelizing does not make you evangelical, but even more pleasing to God.
12. For the grace which God hath in the ..
13. Did you notice that the leaf has marks of earth? At that moment, the cue of your hand jumped here.
14. For the grace that God sees in people is not religion; but love, wisdom and faith.
15. Even agnostics can have that, and that's all people need.

### CHAPTER 3

1. I am not good at fasting, and this is a grave fault coming from one who proclaims the mysteries of God;
2. I ask you to pray for me, so that the Holy Spirit may bless me in fasting with all the necessary gifts.
3. But yesterday I was in a greater fast, and I wrote mighty things;
4. So it is because I can no longer write in this letter without first asking you to ask me to heal all your doubts about God, Bible, existence, etc.

### CHAPTER 4

1. I asked you if you would like to write your doubts, but you denied it by saying that you do not have them.
2. I do not believe this, but I am not a annoying from Jesus;
3. I do not know what to say, but I know why.
4. The devil reads before and wants to disrupt, and commands; but I will not be like him.
5. Follow your heart; the devil forces, but Christians are meek.
6. When I have doubts or sadness, I ask you to reread.
7. If they invent things by this letter, and the other cause strife; I'm happy to just do what I have to.
8. I ask God to give you inner health and peace, and reread ..
9. Curitiba, 06/03/18



