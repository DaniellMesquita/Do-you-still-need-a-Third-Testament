# Teens: 1st letter to Dudu

### CHAPTER 1

1. I address this letter to a person I admire.
2. I write during a fury I have never had and that I have never controlled so much; even while not sleeping.
3. Since we were born, we have a best friend and a worst enemy: ourselves.
4. With our good feelings, "desires", "pleasures", good intentions, loves, intelligence, dreams, joys, courage; We are our best friend.
5. With our bad feelings, dislikes, disappointments, bad intentions, furies, foolishness, sadness, fear; we are our worst enemy.
6. All people have let me down, including my own mother.
7. Not even our entire trust could be deposited.
8. If we trust someone, trust them with suspicion.
9. But not trusting needs to be prioritized.
10. I remember when we came here in January, and I told you about God.
11. At that time it was only a few days before I began to try to understand existence, the senses, the everything, God.
12. But after that, I went astray and became a sinner, even though I knew that I was sinning.
13. And when we sin willingly \(knowing that we are sinning\) it is worse than when we sin without knowing.
14. Those Indians will be resurrected and will live in the house of God;
15. It's not in the Bible, but I know it.
16. I could tell you of your sins \(and I am wrong not to do this\) but I fear that, like me, you will sin by ignoring.
17. It is not the time for this, and I wish you to be happy.
18. You have less sin than I; parts less than most in this house.
19. Truly you are worthy to pray at the table, and God hears you more.
20. From the first day I saw you and your sister, by bike, here in this house; I saw light in you.
21. My mother said, "You are prime cousins, Daniell is shy and would not have done it," but I dropped all my self to simply reverence them.

### CHAPTER 2

1. I have more affinity with dogs, but I am an expert on humans.
2. But I was disappointed with myself by being disappointed with people.
3. I see the potential hidden in people: I know what they can be, what they feel, etc.
4. But in some people I feel a much stronger light.
5. I look crazy writing \(I even write well, even with my present state of nerves\) but this letter can build you;
6. I do not write it by myself.
7. I love to see people happy, and I love to see the dreams and the grace that people see in life.
8. I love people who love, I love to see people in loving contact with nature, I love to know that there are still people who dream, I love to see the human warmth of people who have affection;
9. I love a child's smile, innocent and happy laughter; 2
10. I love people of brightness who see grace in good things and who with much love celebrate existence;
11. I love those who care for one another, those who love that which is weak and helpless;
12. I love sincere people;
13. I love couples who love each other;
14. I love human kindness, faith, and wisdom;
15. I love humanity as the bodies of God.
16. But I can not believe it!
17. The human being has been disappointing and unreliable.
18. Humanity does not cease to corrupt itself and cause fear, hatred and disbelief.
19. How to believe in God?
20. But I do not come to doubt.
21. I come to do for you what I do not do for myself; because I am weak.

### CHAPTER 3

1. When I told you about what made me approach God, I counted confusedly, crying.
2. You were not very stable emotionally either, and you were also very emotional.
3. For a long time I have walked in agnosticism and atheism, not being able to believe that God is the same as the Bible.
4. And God had never shown Himself to me as the same of the Bible;
5. Nor did his creation prove to have a god other than a god who forsook and ignored the suffering of his people.
6. There are thousands of religions, and many that came before the Bible.
7. Others still show the Bible as a feather of plagiarism.
8. How to believe in the God of the Bible?
9. Why did I write this letter to you?
10. For you to laugh at me or despise?
11. Am I not wasting my time?
12. Am I not writing against myself?
13. I have never written with so much discouragement.
14. And I've never felt such hatred, or anger.
15. But I have balanced myself.
16. Imagine being me, walking with the mother I have and seeing everything I see.
17. And being held responsible for everything that has happened.
18. But I can not do foolish things, nor do I want to.
19. I do not write to speak of myself \(and I ask you not to worry about me\) but to build you.
20. You know more than I do about what's going on.
21. But you do not know more about who is above all this, and the sense of it.

### CHAPTER 4

1. Suffering and anger blind, so I will tell you about the things in which even I am having doubts.
2. Because I do not want you to be blind.
3. And God will use you a lot in the future.
4. Is God hard and Jesus good?
5. Are God and Jesus completely different?
6. Between heaven and earth are there more things than vain philosophies?
7. Why does the Bible contradict itself?
8. Jesus said, "I and the Father are one."
9. Then why does God say one thing but then Jesus says another?
10. Jesus and Satan compete; could not God just not have created it?
11. Why did God create evil?
12. Why does God create the wicked and then speak in hell?
13. Why a will and then a new one contradicting?
14. Like the devil, did Jesus contradict the words of God?
15. Eye for eye tooth for tooth \(in Old Testament\) and then Jesus asks to turn the other cheek?
16. What is the point of this?
17. Why do not we just ignore it all and follow our desires?
18. The report about the brothers who died embracing, how God allowed this against children?
19. I do not even know what to write.
20. Sadness and confusion overwhelmed me.
21. **Where is this God who they speaks so much?**
22. **What is the meaning of life?**
23. **What is this God?**
24. **What do we prefer?**
25. What about our bodies? And our soul? And our spirit?
26. Life on Earth or the afterlife?
27. What to believe in?
28. Will we not repent of having invested in the Earth instead of in the things of God?
29. Or is the opposite useless because we have no soul and no god in the heavens?
30. **We men are tormented and confused with so many doubts!**
31. **Why do not we just think "f\*\*k"?**
32. **I want to shout: "F\*\*K!!!!"**.
33. I could not feed myself today.
34. You may not have heard everything I've heard.
35. But you are different from him, and better than him.
36. Do not mind not accompanying you in the church;
37. You are different and God takes pleasure in you for it.
38. Today I held myself, as I held on that day when 3 women were beaten at the same time.
39. And you think it was just two, but I do not write this to quarrels.
40. I do not see you in the same way.
41. You are calm, quiet, virtuous, and good.
42. You save your soul, and work to save others, and not to be shaken by those you can not save.
43. When you complete your studies, being an elder will help you in this.
44. Do not worry, God provides for you.
45. Who gave you such a good job? Above all, the one who helps is God.
46. And I was happy to pray for you \(as I promised\) and God to answer.
47. That same day that I told you that God speaks in thunder, there was a storm, did you notice? That's a sign.

### CHAPTER 5

1. Does God ignore people's suffering?
2. Our life on earth is like a tale, and a test.
3. We experience experiences to form and maintain what we will be in the second life.
4. How to have the product ready without first having the beta stage?
5. The new, the second.
6. How to have a new living testament, and love, without first having an old testament of death and hatred?
7. How to admire evolution and a beautiful history, without first knowing the past of struggles?
8. Old creature, new creature.
9. First testament, second testament.
10. First life, second life.
11. Invisible God, Jesus man.
12. God is in us.
13. He feels what we feel, like a second spirit.
14. This is very difficult to understand!
15. Why does not God want "sensuality"?
16. Because he is on both sides between two people.
17. God is among and within more than seven \(7\) billion people at a time; omniscient, omnipresent, omnipotent.
18. Omniscient: He knows and feels the pains and joys of everything and everything \(God is All and is "All"\);
19. Omnipresent: He is in everyone, and in everything, because he is the All.
20. Omnipotent: your power, thoughts, feelings, and deeds are in everything and achieve everything.
21. I spoke in "sensualities", but a couple that does not betray, is the same flesh, and are allowed to become sensuous.
22. For man hath no power over his body, but his wife hath.
23. While the woman has no power over her body, but her man has it. So it is mutual.
24. There is the first, and the second.
25. The set, and the complement.
26. The head, and the body.
27. God is invisible, so he made the project humanity, to be his body and image.
28. And these images of God have their own self, that God may be more glory.
29. God is many selves in the same self.
30. God needed to speak to mankind, and for humans to strive, he created the Bible instead of simply speaking in the clouds;
31. It is fitting that the Bible be put to the test and confusions between religions and philosophies to merit people.
32. But God also speaks in other ways, but not directly, as a challenge.
33. As the new is better than the old \(in the things of God\) so evil was created for good;
34. Not in the moment, but when the good excels over the bad.
35. And as it is written, they shall say: Death is swallowed up in victory.
36. If you still have doubts, you can ask me, or read the other letters, to discern.
37. Read this letter with her, before you think of a church;
38. Be strong in wisdom and faith!
39. Or if you have a punch for me and return the letter, thank God for handing you a key from heaven.
40. This prophet loves you, and Jesus even more.
41. mssdaniellmesquita@gmail.com
42. fb.me/daniellmesquita
43. +55 41 9 9866-9524

### CHAPTER 6

1. I will abandon "Christianity" and follow another religion if:
2. Also speak that God is eternal and infinite;
3. Also speak that God is invisible but real;
4. Also speak that all cogitations of man is that there is no God;
5. Also speak that love is the bond of perfection;
6. To also say that in fear there is not the perfection of love;
7. Also speak that man is the image and likeness of God;
8. Also speak that God is love;
9. Also speak that God has a first fruits of all his creatures \(Jesus\);
10. Also speak that God is perfect;
11. Also speak that wisdom is the main thing;
12. Also speak that God has made the wisdom of men foolish;
13. Also speak that the thoughts of God can not be counted before him;
14. Also speak that the mercy of God is greater than the truth;
15. Also speak that mercy shall triumph of judgment;
16. Also speak that the brethren of the poor shall be their enemies;
17. Also speak that many will be called and few chosen;
18. Also speak that at the end of the world there will be famine, plagues and earthquakes;
19. Also speak that there is a third heaven;
20. Also speak that there will be signs in the heavens;
21. Also speak that all sounds have a meaning;
22. Also speak that thunders are the voice of God;
23. Also speak that there is no one righteous, not even one;
24. Also speak that "all" is vanity;
25. Also speak that God is not a god of confusion;
26. Also speak that evil men are vessels of wrath, and with much patience God supports;
27. Also speak that our life is like a tale;
28. Also say that not all will pay here on Earth;
29. Also speak that the good pay before the bad;
30. Also speak that those who live a lot can not be proud;
31. Also speak that the devil is the god of that age;
32. Also speak that gossips bring gifts and flatter;
33. Also speak that hell is never satisfied;
34. Also speak that God is the father of orphans and the widow's Judge;
35. Also speak that money is the root of "all" a kind of evil;
36. Also say that we can not serve two masters;
37. Also speak that all would know the Lord.
38. The religion that has all this, and other important things it has in the Bible, is perfect.
39. And for this reason Christianity is the greatest religion on earth; and we had him as his first love, and we grew up with him.
40. Curitiba, 06/02/18

